# rolex-tudor-h5

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.1.5.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## SEO

https://medium.com/@maciejtreder/angular-server-side-rendering-with-ng-toolkit-universal-c08479ca688

使用 @toolkit/universal 实现服务端渲染
这个解决方案就是 Angular Universal，NodeJS的一种 Angular 扩展，它在服务器上启动应用程序，并根据收入请求生成完整的 HTML 响应。现在，从用户请求到渲染和交互的Web应用程序的流程看起来如下

1. 用户请求网页
2. 请求发送到服务端
3. NodeJS生成 HTML 并将其发送到浏览器 
4. 浏览器从HTML呈现视图，并立即将其显示给用户，同时执行JavaScript。 
5. 当JavaScript完成应用程序的引导时，它用 Angular 应用程序更改HTML渲染视图。 
6. 用户看到完全可交互的 Angular 应用

运行这个应用

1. npm run build:prod
2. npm run server

在单独的终端窗口检查一下效果
$ curl localhost:8080

## 使用 @ng-toolkit/serverless 部署你的 Angular 应用在 Firebase / AWS Lambda / Google Cloud Functions

https://medium.com/@maciejtreder/angular-serverless-a713e86ea07a

## 移动端适配：font-size设置的思考

* https://www.cnblogs.com/axl234/p/5156956.html

## 微信用户同意授权，获取code url

* flow1 https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxf9f5d616be1cdff7&redirect_uri=http%3A%2F%2Farba.nat300.top%2Fbestv%2Fwechat%2FfindUserInfo&response_type=code&scope=snsapi_userinfo&state=flow1#wechat_redirect
* flow2 https://open.weixin.qq.com/connect/oauth2/authorize?appid=wxf9f5d616be1cdff7&redirect_uri=http%3A%2F%2Farba.nat300.top%2Fbestv%2Fwechat%2FfindUserInfo&response_type=code&scope=snsapi_userinfo&state=flow2#wechat_redirect

## Angular路由复用策略 (切换页面内容不丢失，保持原来状态)

* 如果不想使用路由复用策略, 如: this.router.navigate(['rolex-stores/detail', id], {queryParams: { keep: 0 } }); or http://192.168.24.226:4200/rolex-stores?keep=0

## api文档

* http://47.107.60.139/bestv/swagger-ui.html

## test view

* http://arba.mynatapp.cc/rolex_tudor_h5/rolex-stores
* http://arba.mynatapp.cc/rolex_tudor_h5/tudor-stores