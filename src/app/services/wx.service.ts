import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { API_SERVE_URL } from '../config/constants';
import { JWeiXinService } from 'ngx-weui';
declare const wx: any;

/**
 * 微信JS-SDK服务器
 */
@Injectable({
    providedIn: 'root'
})
export class WXService {

    constructor(
        private http: HttpClient,
        private jWeiXinService: JWeiXinService
    ) { }

    loadJSSDK(jweixinUrl: string) {
        this.jWeiXinService.get(jweixinUrl).then((res) => {
            if (!res) {
                console.log('jweixin.js 加载失败');
            } else {
                console.log('jweixin.js 加载成功');
            }
        });
    }

    config(apiUrl: string, url: string, jsApiList: string[] = [], shareData?: any, hideMenuItems: string[] = []): Observable<boolean> {
        return new Observable(observer => {
            wx.ready(() => {
                console.log('JSSDK配置成功');
                // if (hideMenuItems) {
                //     wx.hideMenuItems({
                //         // tslint:disable-next-line:max-line-length
                // tslint:disable-next-line:max-line-length
                //         menuList: hideMenuItems // ['menuItem:copyUrl', 'menuItem:openWithQQBrowser', 'menuItem:openWithSafari', 'menuItem:share:facebook', 'menuItem:favorite', 'menuItem:share:email'] // 要显示的菜单项，所有menu项见附录3
                //     });
                // }

                // if (shareData) {
                //     wx.onMenuShareAppMessage({
                //         title: shareData.title, // 分享标题
                //         desc: shareData.desc, // 分享描述
                //         link: shareData.link, // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
                //         imgUrl: shareData.imgUrl // 分享图标
                //     });
                //     wx.onMenuShareTimeline({
                //         title: shareData.title, // 分享标题
                //         link: shareData.link, // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
                //         imgUrl: shareData.imgUrl // 分享图标
                //     });
                //     wx.onMenuShareQQ({
                //         title: shareData.title, // 分享标题
                //         desc: shareData.desc, // 分享描述
                //         link: shareData.link, // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
                //         imgUrl: shareData.imgUrl // 分享图标
                //     });
                //     wx.onMenuShareWeibo({
                //         title: shareData.title, // 分享标题
                //         desc: shareData.desc, // 分享描述
                //         link: shareData.link, // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
                //         imgUrl: shareData.imgUrl // 分享图标
                //     });
                //     wx.onMenuShareQZone({
                //         title: shareData.title, // 分享标题
                //         desc: shareData.desc, // 分享描述
                //         link: shareData.link, // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
                //         imgUrl: shareData.imgUrl // 分享图标
                //     });
                // }
                observer.next(true);
            });
            wx.error((error) => {
                // alert(JSON.stringify(error));
                observer.error('config 注册失败');
            });
            this.http.post(`${API_SERVE_URL}/${apiUrl}`, { url: url })
                .subscribe((res: any) => {
                    // console.log(res);
                    wx.config({
                        debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
                        appId: res.appId, // 必填，公众号的唯一标识
                        timestamp: res.timestamp, // 必填，生成签名的时间戳
                        nonceStr: res.nonceStr, // 必填，生成签名的随机串
                        signature: res.signature, // 必填，签名
                        jsApiList: jsApiList // 必填，需要使用的JS接口列表
                    });
                }, error => {
                    observer.error('无法获取签名数据');
                });
        });
    }

    closeWindow() {
        wx.closeWindow();
    }

    getLocation(): Observable<any> {
        return new Observable(observer => {
            wx.getLocation({
                // type: type, // 默认为wgs84的gps坐标，如果要返回直接给openLocation用的火星坐标，可传入'gcj02'
                success: (res) => {
                    alert('wx.getLocation: ' + JSON.stringify(res));
                    observer.next(res);
                },
                fail: (error) => {
                    observer.error(error);
                }
            });
        });
    }
}
