import { Injectable } from '@angular/core';
// tslint:disable-next-line:max-line-length
import { HttpHandler, HttpHeaderResponse, HttpInterceptor, HttpProgressEvent, HttpRequest, HttpResponse, HttpSentEvent, HttpUserEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

@Injectable()
export class HttpInterceptorService implements HttpInterceptor {

    constructor(
    ) {
    }

    intercept(
        req: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpSentEvent | HttpHeaderResponse | HttpProgressEvent | HttpResponse<any> | HttpUserEvent<any>> {
        // console.log('interceptor');
        let authReq = req.clone({
            setHeaders: {
                'content-type': 'application/json; charset=utf-8'
                // 'Authorization': `Bearer 12345678`
            }
        });
        // console.log(authReq);
        // if (this.globalData.token) {
        //     console.log(this.globalData.token);
        // }

        return next
            .handle(authReq)
            .pipe(
                tap((event: any) => {
                    // if (event instanceof HttpResponse) {
                    //     console.log('succeed');
                    //     return Observable.create(observer => observer.next(event));
                    // } else {
                    //     return Observable.create(observer => observer.error({error: '系统出错！！！'}));
                    // }
                    return Observable.create(observer => observer.next(event));
                }),
                catchError((res: HttpResponse<any>) => {
                    switch (res.status) {
                        case 401:
                            // 权限处理
                            break;
                        case 404:
                            break;
                    }
                    // 以错误的形式结束本次请求
                    return Observable.create(observer => observer.error(res));
                })
            );
    }
}
