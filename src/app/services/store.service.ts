import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Jsonp } from '@angular/http';
import { Observable } from 'rxjs';
import { API_SERVE_URL, AK } from '../config/constants';

@Injectable({
    providedIn: 'root'
})
export class StoreService {

    constructor(
        private http: HttpClient,
        private jsonp: Jsonp
    ) {
    }

    findStores(brand: number, lat: number, lng: number): Observable<any> {
        return new Observable(observer => {
            // let httpParams = new HttpParams();
            // httpParams.set('cellphone', phone);
            this.http.post(`${API_SERVE_URL}/RT/rt/findStores`, { brand: brand, lat: lat, lng: lng })
                .subscribe(res => {
                    observer.next(res);
                }, error => {
                    observer.error(error);
                });
        });
    }

    findStoreById(retailerId: string): Observable<any> {
        return new Observable(observer => {
            // let httpParams = new HttpParams();
            // httpParams.set('cellphone', phone);
            this.http.get(`${API_SERVE_URL}/RT/rt/map/${retailerId}`, {})
                .subscribe(res => {
                    observer.next(res);
                }, error => {
                    observer.error(error);
                });
        });
    }

    findStoreByName(name: string, brand: number, lat: number, lng: number): Observable<any> {
        return new Observable(observer => {
            // let httpParams = new HttpParams();
            // httpParams.set('cellphone', phone);
            this.http.post(`${API_SERVE_URL}/RT/rt/findStoresByName`, { name: name, brand: brand, lat: lat, lng: lng })
                .subscribe(res => {
                    observer.next(res);
                }, error => {
                    observer.error(error);
                });
        });
    }

    getCurrentPosition(): Observable<any> {
        return new Observable(observer => {
            // httpParams.set('cellphone', phone);
            // this.http.get(`http://api.map.baidu.com/location/ip?ak=jPeKjwAa5KAgstT6OQWkP9tR&coor=bd09ll`, {})
            //     .subscribe(res => {
            //         observer.next(res);
            //     }, error => {
            //         observer.next(error);
            //     });
            if (navigator.geolocation) { // 用浏览器获取坐标地址
                let options = {
                    enableHighAccuracy: true,
                    timeout: 10000,
                    maximumAge: 0
                };
                navigator.geolocation.getCurrentPosition(
                    (position) => {
                        alert(JSON.stringify(position));
                        observer.next(position);
                    },
                    (error) => {
                        alert(JSON.stringify({ msg: '浏览器地理定位失败' }));
                        observer.error({ msg: '浏览器地理定位失败' });
                    }
                    , options);
            } else {
                alert(JSON.stringify({ msg: '浏览器不支持地理定位' }));
                observer.error({ msg: '浏览器不支持地理定位' });
            }
        });
    }

    mapSearch(query: string, lat: string, lng: string): Observable<any> {
        return new Observable(observer => {
            // tslint:disable-next-line:max-line-length
            let apiUrl = `http://api.map.baidu.com/place/v2/search?query=${query}&location=${lat},${lng}&ak=${AK}&output=json&callback=JSONP_CALLBACK`;
            // console.log(apiUrl);
            this.jsonp.request(apiUrl)
                .subscribe(res => {
                    observer.next(res.json());
                }, error => {
                    observer.error(error);
                });
        });
    }

    changePosition(lat: string, lng: string): Observable<any> {
        return new Observable(observer => {
            // tslint:disable-next-line:max-line-length
            let apiUrl = `http://api.map.baidu.com/geoconv/v1/?coords=${lng},${lat}&from=1&to=5&ak=${AK}&output=json&callback=JSONP_CALLBACK`;
            // console.log(apiUrl);
            this.jsonp.request(apiUrl)
                .subscribe(res => {
                    // console.log(res.json());
                    observer.next(res.json());
                }, error => {
                    observer.error(error);
                });
        });
    }
}
