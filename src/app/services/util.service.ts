import { Injectable } from '@angular/core';
import { ToptipsService } from 'ngx-weui';

@Injectable({
    providedIn: 'root'
})
export class UtilService {

    constructor(
        private toptipsService: ToptipsService,
    ) {
    }

    isWeixin(): boolean {
        let ua: string = navigator.userAgent.toLowerCase();
        if (ua.indexOf('micromessenger') > -1) {
            return true;
        } else {
            return false;
        }
    }

    toptipDefault(text: string, time?: number) {
        this.toptipsService.default(text, time);
    }

    toptipInfo(text: string, time?: number) {
        this.toptipsService.info(text, time);
    }

    toptipPrimary(text: string, time?: number) {
        this.toptipsService.primary(text, time);
    }

    toptipSuccess(text: string, time?: number) {
        this.toptipsService.success(text, time);
    }

    toptipWarn(text: string, time?: number) {
        this.toptipsService.warn(text, time);
    }

}
