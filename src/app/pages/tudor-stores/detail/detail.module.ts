import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TudorStoreDetailRoutingModule } from './detail-routing.module';
import { TudorStoreDetailComponent } from './detail.component';
import { BackButtonModule } from '../../../components';

@NgModule({
  imports: [
    CommonModule,
    TudorStoreDetailRoutingModule,
    BackButtonModule
  ],
  declarations: [TudorStoreDetailComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class TudorStoreDetailModule { }
