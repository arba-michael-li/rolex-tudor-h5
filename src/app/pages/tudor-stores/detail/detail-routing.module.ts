import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TudorStoreDetailComponent } from './detail.component';

const routes: Routes = [
  {
    path: '',
    component: TudorStoreDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TudorStoreDetailRoutingModule { }
