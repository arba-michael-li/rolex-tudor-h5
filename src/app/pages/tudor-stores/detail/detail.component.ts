import { Component, OnInit, AfterViewInit, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { WINDOW } from '@ng-toolkit/universal';
import { Title, Meta } from '@angular/platform-browser';
import { StoreService } from '../../../services';
// import { CUSTOM_MAP_CONFIG } from '../../../config/custom-map-config-tudor';
import * as $ from 'jquery';
declare let BMap;

@Component({
    selector: 'app-tudor-stores-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.scss']
})
export class TudorStoreDetailComponent implements OnInit, AfterViewInit {
    showInfo = false;
    retailerId: string;
    store: any;

    constructor(
        title: Title,
        meta: Meta,
        private route: ActivatedRoute,
        private router: Router,
        private storeService: StoreService,
        @Inject(WINDOW) private window: Window
    ) {
        $(`head link[rel="icon"]`).attr('href', 'assets/images/favicon-tudor.ico');
        title.setTitle('帝舵表零售商地址');
        meta.updateTag({ name: 'keywords', content: 'TUDOR, 帝舵, 帝舵表, 名表, 瑞士表, 瑞士制表品牌, 男表, 男装表, 帝舵表特约零售商, 帝舵表分支机构及服务中心' });
        meta.updateTag({ name: 'description', content: '寻找最就近的帝舵表特约零售商，选购及保养您的帝舵奢华瑞士腕表。' });
    }

    ngOnInit() {

    }

    ngAfterViewInit() {
        this.route.params.subscribe((params) => {
            // console.log(params);
            this.retailerId = params.retailerId;
            setTimeout(() => {
                $(`#map`).height($(`body`).height() - $(`.page__bd`).height());
                this.onReady();
            }, 1000);
        });
    }

    onReady() {
        $(document).ready(() => { // 在文档加载完毕后执行
            this.findStore();
        });
    }

    linePlanning(retailerId) {
        this.router.navigate(['tudor-stores/line-planning', retailerId], { queryParams: { keep: 0 } });
    }

    findStore() {
        this.storeService.findStoreById(this.retailerId)
            .subscribe(res => {
                // console.log(res);
                this.store = res;
                this.initMap();
                this.showInfo = true;
            }, error => {

            });
    }

    initMap() {
        // 百度地图API功能
        let map = new BMap.Map('map', { minZoom: 4 });    // 创建Map实例
        map.centerAndZoom(new BMap.Point(this.store.storeLongitude, this.store.storeLatitude), 16);  // 初始化地图,设置中心点坐标和地图级别
        map.enableScrollWheelZoom();
        // map.setMapStyleV2({ styleJson: CUSTOM_MAP_CONFIG });
        let pt = new BMap.Point(this.store.storeLongitude, this.store.storeLatitude);
        let marker = new BMap.Marker(pt, { icon: new BMap.Icon('assets/images/tudor-pin.svg', new BMap.Size(32, 35)) });
        map.addOverlay(marker);
    }

    toCall(number) {
        this.window.location.href = `tel://${number}`;
    }

}
