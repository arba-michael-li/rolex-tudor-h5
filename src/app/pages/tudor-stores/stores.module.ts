import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TudorStoresRoutingModule } from './stores-routing.module';
import { TudorStoresComponent } from './stores.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    TudorStoresRoutingModule
  ],
  declarations: [TudorStoresComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class TudorStoresModule { }
