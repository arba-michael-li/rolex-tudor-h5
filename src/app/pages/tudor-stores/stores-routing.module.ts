import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TudorStoresComponent } from './stores.component';

const routes: Routes = [
  {
    path: '',
    component: TudorStoresComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TudorStoresRoutingModule { }
