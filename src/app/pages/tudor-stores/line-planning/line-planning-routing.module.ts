import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TudorStoreLinePlanningComponent } from './line-planning.component';

const routes: Routes = [
  {
    path: '',
    component: TudorStoreLinePlanningComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TudorStoreLinePlanningRoutingModule { }
