import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TudorStoreLinePlanningRoutingModule } from './line-planning-routing.module';
import { TudorStoreLinePlanningComponent } from './line-planning.component';
import { BackButtonModule } from '../../../components';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    TudorStoreLinePlanningRoutingModule,
    BackButtonModule
  ],
  declarations: [TudorStoreLinePlanningComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class TudorStoreLinePlanningModule { }
