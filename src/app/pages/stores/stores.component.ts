import { Component, OnInit, AfterViewInit, Inject, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { Title, Meta } from '@angular/platform-browser';
import { LOCAL_STORAGE, WINDOW } from '@ng-toolkit/universal';
import * as $ from 'jquery';
import { debounceTime } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { StoreService, WXService } from '../../services';
// import { CUSTOM_MAP_CONFIG } from '../../config/custom-map-config';
import * as _ from 'lodash';
declare let BMap;

@Component({
    selector: 'app-stores',
    templateUrl: './stores.component.html',
    styleUrls: ['./stores.component.scss']
})
export class StoresComponent implements OnInit, AfterViewInit {
    searchText: string;
    searchTextStream: Subject<string> = new Subject<string>();
    map: any;
    navId = 0;
    markerArry = [];
    showStoreDialog = false;
    showSearchResult = false;
    searching = false;
    stores: any[] = [];
    store: any;
    showNumber = 4;
    currentPosition = {
        point: {
            lat: null,
            lng: null
        }
    };
    searchResultItems = {
        addressList: [],
        nameList: []
    };
    searchResultBoxTop = 0;
    searchResultBoxHeight = 200;
    q: string;

    constructor(
        title: Title,
        meta: Meta,
        private ngZone: NgZone,
        @Inject(LOCAL_STORAGE) private localStorage: any,
        @Inject(WINDOW) private window: Window,
        private router: Router,
        private storeService: StoreService,
        private wxService: WXService
    ) {
        $(`head link[rel="icon"]`).attr('href', 'favicon.ico');
        title.setTitle('劳力士零售商地址');
        meta.updateTag({ name: 'keywords', content: 'Rolex, 劳力士腕表, Rolex retailers, 选购劳力士腕表, 劳力士零售商, 劳力士零售商地址, 劳力士真品' });
        meta.updateTag({ name: 'description', content: '唯有劳力士授权的特约零售商，方能提供销售及保养劳力士腕表的服务。于劳力士官方网站寻找最就近您的劳力士零售商。' });
    }

    ngOnInit() {
    }

    ngAfterViewInit() {
        this.onReady();
        setTimeout(() => {
            $(`#map`).height($(`body`).height() - $(`.page__bd`).height());
            this.searchResultBoxTop = $(`.page__bd`).height() + 54;
            this.searchResultBoxHeight = $(`body`).height() - $(`.page__bd`).height() - 64;
            // this.initMap();
        }, 1000);
        this.searchTextStream
            .pipe(
                debounceTime(500)
            ).subscribe(searchText => {
                if (searchText === null || typeof (searchText) === 'undefined' || searchText.trim() === '') {
                    this.showSearchResult = false;
                    this.q = null;
                } else if (searchText !== this.q) {
                    this.showSearchResult = true;
                    this.q = searchText;
                    this.searchStores(this.q);
                }
            });
    }

    onReady() {
        $(document).ready(() => { // 在文档加载完毕后执行
            this.getLocation();
            // this.wxInit();
            $(`#list-box`).hide();
            $(`#map-box`).show();
            $(`.page`).scroll(() => {// 开始监听滚动条
                // 获取当前滚动条高度
                let scrollTop = $(`.page`).scrollTop();
                if ((scrollTop + $(`#nav-menu`).height()) > $(`.page__bd`).height()) {
                    $(`#nav-menu`).addClass('fixed-top');
                } else {
                    $(`#nav-menu`).removeClass('fixed-top');
                }
            });
        });
    }

    wxInit() {
        // tslint:disable-next-line:max-line-length
        this.wxService.config('RT/jsapi/initrolex', this.window.location.href, ['getLocation'], null, [])
            .subscribe(res => {
                // console.log(res);
                this.wxGetLocation();
            }, error => {
                // console.log(error);
                this.getLocation();
            });
    }

    wxGetLocation() {
        this.wxService.getLocation()
            .subscribe(res => {
                // console.log('wx getLocation');
                // console.log(res);
                // alert(JSON.stringify(res));
                this.currentPosition = {
                    point: {
                        lat: res.latitude,
                        lng: res.longitude
                    }
                };
                this.changePosition();
            }, error => {
                // console.log(error);
                this.getLocation();
            });
    }

    changePosition() {
        this.storeService.changePosition(this.currentPosition.point.lat, this.currentPosition.point.lng)
            .subscribe(res => {
                this.currentPosition.point.lat = res.result[0].y;
                this.currentPosition.point.lng = res.result[0].x;
                this.findStores();
            }, error => {
            });
    }

    getLocation() {
        new BMap.Geolocation().getCurrentPosition(
            (position) => {
                console.log(position);

                // alert(JSON.stringify(position));
                // alert('BMap.Geolocation().getCurrentPosition: ' + JSON.stringify(position));
                // console.log('BMap getCurrentPosition');
                if (position !== null) {
                    this.currentPosition = position;
                }
                // this.currentPosition = position;
                this.findStores();
            },
            {
                enableHighAccuracy: true,
                timeout: 600000,
                SDKLocation: true
            }
        );
    }

    findStores() {
        this.storeService.findStores(1, this.currentPosition.point.lat, this.currentPosition.point.lng)
            .subscribe(res => {
                // console.log(res);
                this.stores = res;
                this.initMap();
            }, error => {
            });
    }

    setShowNumber() {
        this.showNumber += 4;
    }

    searchChanged(args) {
        // console.log(this.searchText);
        this.searchTextStream.next(this.searchText);
    }

    initMap() {
        // 百度地图API功能
        this.map = new BMap.Map('map', { minZoom: 1 });    // 创建Map实例
        // tslint:disable-next-line:max-line-length
        this.map.centerAndZoom(new BMap.Point(this.stores[0].storeLongitude, this.stores[0].storeLatitude), this.currentPosition.point.lat ? 16 : 4);  // 初始化地图,设置中心点坐标和地图级别
        this.map.enableScrollWheelZoom();
        // this.map.disableDoubleClickZoom();
        // this.map.setMapStyleV2({ styleJson: CUSTOM_MAP_CONFIG });
        this.initMarkers();
    }

    initMarkers() {
        let pt = null;
        this.stores.forEach((item, i) => {
            pt = new BMap.Point(item.storeLongitude, item.storeLatitude);
            // tslint:disable-next-line:max-line-length
            let marker = new BMap.Marker(pt, { icon: new BMap.Icon('assets/images/rolex-pin.svg', new BMap.Size(32, 35)) });
            this.map.addOverlay(marker);
            this.markerArry[i] = marker;
            this.markerArry[i].addEventListener('click', (e) => {
                // console.log(e);
                // console.log(e.target.point);
                this.ngZone.run(() => {
                    this.store = _.find(this.stores, { storeLatitude: e.target.point.lat, storeLongitude: e.target.point.lng });
                    // console.log(this.store);
                    this.showStoreDialog = true;
                });
            });
        });
    }

    onBlur() {
    }

    onFocus() {
        // tslint:disable-next-line:max-line-length
        if (this.searchResultItems.addressList.length > 0 || this.searchResultItems.nameList.length > 0) {
            this.showSearchResult = true;
        }
        this.showStoreDialog = false;
    }

    showDialog(id) {
        this.store = _.find(this.stores, { id: id });
        this.showSearchResult = false;
        this.showStoreDialog = true;

        this.map.centerAndZoom(new BMap.Point(this.store.storeLongitude, this.store.storeLatitude), 16);  // 设置中心点坐标和地图级别
    }

    tapNav(index) {
        this.showStoreDialog = false;
        this.navId = index;
        if (index === 0) {
            $(`#list-box`).hide();
            $(`#map-box`).show();
        } else if (index === 1) {
            $(`#map-box`).hide();
            $(`#list-box`).show();
        }
        // $(`.page`).scrollTop($(`.page__bd`).height() - $(`#nav-menu`).height());
    }

    linePlanning(retailerId) {
        this.localStorage.setItem('rolex-stores-scrollTop', $(`.page`).scrollTop());
        this.router.navigate(['rolex-stores/line-planning', retailerId], { queryParams: { keep: 0 } });
    }

    toDetail(retailerId) {
        this.localStorage.setItem('rolex-stores-scrollTop', $(`.page`).scrollTop());
        this.router.navigate(['rolex-stores/detail', retailerId]);
    }

    moveLocation() {
        let allOverlay = this.map.getOverlays();
        // console.log(allOverlay);
        allOverlay.forEach(item => {
            if (item.point && (item.point.lat === this.currentPosition.point.lat)) {
                this.map.removeOverlay(item);
                return false;
            }
        });
        // 创建查询对象
        let geolocation = new BMap.Geolocation();

        // 调用getCurrentPosition函数
        geolocation.getCurrentPosition((position) => {
            if (position !== null) {
                this.currentPosition = position;
            }
            // console.log(position);
            // 如果查询成功
            this.map.setZoom(16);
            // 根据point对象创建标记遮挡物，并添加到地图中
            // tslint:disable-next-line:max-line-length
            // tslint:disable-next-line:max-line-length
            let marker = new BMap.Marker(position.point, { icon: new BMap.Icon('assets/images/rolex-myself.svg', new BMap.Size(100, 100)) });
            this.map.addOverlay(marker);

            // 将地图中心设置为获得的当前位置
            this.map.panTo(position.point);
        });
    }

    // moveLocation() {
    //     if (!this.currentPosition.point.lat || !this.currentPosition.point.lng) {
    //         return;
    //     }
    //     let allOverlay = this.map.getOverlays();
    //     // console.log(allOverlay);
    //     allOverlay.forEach(item => {
    //         if (item.point && (item.point.lat === this.currentPosition.point.lat)) {
    //             this.map.removeOverlay(item);
    //             return false;
    //         }
    //     });
    //     setTimeout(() => {
    //         this.map.setZoom(16);
    //         // 根据point对象创建标记遮挡物，并添加到地图中
    // tslint:disable-next-line:max-line-length
    //         let marker = new BMap.Marker(this.currentPosition.point, { icon: new BMap.Icon('assets/images/rolex-myself.svg', new BMap.Size(100, 100)) });
    //         this.map.addOverlay(marker);

    //         // 将地图中心设置为获得的当前位置
    //         this.map.panTo(new BMap.Point(this.currentPosition.point.lng, this.currentPosition.point.lat));
    //     }, 500);

    // }

    searchStores(q) {
        this.searching = true;
        this.storeService.findStoreByName(q, 1, this.currentPosition.point.lat, this.currentPosition.point.lng)
            .subscribe(res => {
                // console.log(res);
                this.searchResultItems = res;
                this.searching = false;
            }, error => {
                this.searching = false;
            });
    }

    toCall(number) {
        this.window.location.href = `tel://${number}`;
    }
}
