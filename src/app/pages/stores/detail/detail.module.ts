import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreDetailRoutingModule } from './detail-routing.module';
import { StoreDetailComponent } from './detail.component';
import { BackButtonModule } from '../../../components';

@NgModule({
  imports: [
    CommonModule,
    StoreDetailRoutingModule,
    BackButtonModule
  ],
  declarations: [StoreDetailComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class StoreDetailModule { }
