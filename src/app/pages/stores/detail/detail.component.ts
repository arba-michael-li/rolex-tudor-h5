import { Component, OnInit, AfterViewInit, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { WINDOW } from '@ng-toolkit/universal';
import { Title, Meta } from '@angular/platform-browser';
import { StoreService } from '../../../services';
import * as $ from 'jquery';
// import { CUSTOM_MAP_CONFIG } from '../../../config/custom-map-config';
declare let BMap;

@Component({
    selector: 'app-stores-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.scss']
})
export class StoreDetailComponent implements OnInit, AfterViewInit {
    showInfo = false;
    retailerId: string;
    store: any;

    constructor(
        title: Title,
        meta: Meta,
        private route: ActivatedRoute,
        private router: Router,
        private storeService: StoreService,
        @Inject(WINDOW) private window: Window
    ) {
        $(`head link[rel="icon"]`).attr('href', 'favicon.ico');
        title.setTitle('劳力士零售商地址');
        meta.updateTag({ name: 'keywords', content: 'Rolex, 劳力士腕表, Rolex retailers, 选购劳力士腕表, 劳力士零售商, 劳力士零售商地址, 劳力士真品' });
        meta.updateTag({ name: 'description', content: '唯有劳力士授权的特约零售商，方能提供销售及保养劳力士腕表的服务。于劳力士官方网站寻找最就近您的劳力士零售商。' });
    }

    ngOnInit() {

    }

    ngAfterViewInit() {
        this.route.params.subscribe((params) => {
            // console.log(params);
            this.retailerId = params.retailerId;
            setTimeout(() => {
                $(`#map`).height($(`body`).height() - $(`.page__bd`).height());
                this.onReady();
            }, 1000);
        });
    }

    onReady() {
        $(document).ready(() => { // 在文档加载完毕后执行
            this.findStore();
        });
    }

    linePlanning(retailerId) {
        this.router.navigate(['rolex-stores/line-planning', retailerId], { queryParams: { keep: 0 } });
    }

    findStore() {
        this.storeService.findStoreById(this.retailerId)
            .subscribe(res => {
                // console.log(res);
                this.store = res;
                this.initMap();
                this.showInfo = true;
            }, error => {

            });
    }

    initMap() {
        // 百度地图API功能
        let map = new BMap.Map('map', { minZoom: 4 });    // 创建Map实例
        map.centerAndZoom(new BMap.Point(this.store.storeLongitude, this.store.storeLatitude), 16);  // 初始化地图,设置中心点坐标和地图级别
        map.enableScrollWheelZoom();
        // map.setMapStyleV2({ styleJson: CUSTOM_MAP_CONFIG });
        let pt = new BMap.Point(this.store.storeLongitude, this.store.storeLatitude);
        let marker = new BMap.Marker(pt, { icon: new BMap.Icon('assets/images/rolex-pin.svg', new BMap.Size(32, 35)) });
        map.addOverlay(marker);
    }

    toCall(number) {
        this.window.location.href = `tel://${number}`;
    }

}
