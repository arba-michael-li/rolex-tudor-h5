import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { StoresRoutingModule } from './stores-routing.module';
import { StoresComponent } from './stores.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    StoresRoutingModule
  ],
  declarations: [StoresComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class StoresModule { }
