import { Component, OnInit, AfterViewInit, NgZone, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Title, Meta } from '@angular/platform-browser';
import { WINDOW } from '@ng-toolkit/universal';
import { StoreService, WXService } from '../../../services';
import { debounceTime } from 'rxjs/operators';
import { Subject } from 'rxjs';
import * as $ from 'jquery';
declare let BMap;

@Component({
    selector: 'app-stores-line-planning',
    templateUrl: './line-planning.component.html',
    styleUrls: ['./line-planning.component.scss']
})
export class StoreLinePlanningComponent implements OnInit, AfterViewInit {
    searchText: string;
    searchTextStream: Subject<string> = new Subject<string>();
    showSearchResult = false;
    searchResultItems: any[] = [];
    q: string;
    searching = false;
    map: any;
    navId = 0;
    retailerId: string;
    store: any;
    currentPosition = {
        lat: null,
        lng: null,
        city: null
    };
    location: any;
    hasRoute = true;

    constructor(
        title: Title,
        meta: Meta,
        private route: ActivatedRoute,
        private storeService: StoreService,
        private wxService: WXService,
        private ngZone: NgZone,
        @Inject(WINDOW) private window: Window
    ) {
        $(`head link[rel="icon"]`).attr('href', 'favicon.ico');
        title.setTitle('劳力士零售商地址');
        meta.updateTag({ name: 'keywords', content: 'Rolex, 劳力士腕表, Rolex retailers, 选购劳力士腕表, 劳力士零售商, 劳力士零售商地址, 劳力士真品' });
        meta.updateTag({ name: 'description', content: '唯有劳力士授权的特约零售商，方能提供销售及保养劳力士腕表的服务。于劳力士官方网站寻找最就近您的劳力士零售商。' });
    }

    ngOnInit() {

    }

    ngAfterViewInit() {
        this.route.params.subscribe((params) => {
            // console.log(params);
            this.retailerId = params.retailerId;
            this.onReady();
        });

        this.searchTextStream
            .pipe(
                debounceTime(500)
            ).subscribe(searchText => {
                if (searchText === null || typeof (searchText) === 'undefined' || searchText.trim() === '') {
                    this.showSearchResult = false;
                    this.q = null;
                } else if (searchText !== this.q) {
                    this.showSearchResult = true;
                    this.q = searchText;
                    this.searchStores(this.q);
                }
            });
    }

    searchChanged(args) {
        // console.log(this.searchText);
        this.searchTextStream.next(this.searchText);
    }

    onBlur() {
    }

    onFocus() {
        if (this.searchResultItems.length > 0) {
            this.showSearchResult = true;
        }
    }

    searchStores(q) {
        this.searching = true;
        this.storeService.mapSearch(q, this.currentPosition.lat, this.currentPosition.lng)
            .subscribe(res => {
                this.showSearchResult = true;
                this.searching = false;
                this.searchResultItems = res.results;
            }, error => {
                this.searching = false;
            });
    }

    onReady() {
        $(document).ready(() => { // 在文档加载完毕后执行
            // let geolocation = new BMap.Geolocation();
            // // 调用getCurrentPosition函数
            // geolocation.getCurrentPosition(
            //     (position) => {
            //         // console.log(position);
            //         this.currentPosition = {
            //             lng: position.point.lng,
            //             lat: position.point.lat,
            //             city: position.address.city
            //         };
            //         this.getLocation();
            //         this.findStore();
            //     },
            //     { enableHighAccuracy: true }
            // );
            this.getCurrentPosition();
            // this.wxInit();
        });
    }

    wxInit() {
        // tslint:disable-next-line:max-line-length
        this.wxService.config('RT/jsapi/initrolex', this.window.location.href, ['getLocation'], null, [])
            .subscribe(res => {
                // console.log(res);
                this.wxGetLocation();
            }, error => {
                // console.log(error);
                this.getCurrentPosition();
            });
    }

    wxGetLocation() {
        this.wxService.getLocation()
            .subscribe(res => {
                // console.log('wx getLocation');
                // console.log(res);
                this.currentPosition = {
                    lat: res.latitude,
                    lng: res.longitude,
                    city: null
                };
                // this.getLocation();
                // this.findStore();
                this.changePosition();
            }, error => {
                // console.log(error);
                this.getCurrentPosition();
            });
    }

    changePosition() {
        this.storeService.changePosition(this.currentPosition.lat, this.currentPosition.lng)
            .subscribe(res => {
                this.currentPosition.lat = res.result[0].y;
                this.currentPosition.lng = res.result[0].x;
                this.getLocation();
                this.findStore();
            }, error => {
            });
    }

    getCurrentPosition() {
        new BMap.Geolocation().getCurrentPosition(
            (position) => {
                // console.log('BMap getCurrentPosition');
                // console.log(position);
                if (position !== null) {
                    this.currentPosition = {
                        lng: position.point.lng,
                        lat: position.point.lat,
                        city: position.address.city
                    };
                }
                this.getLocation();
                this.findStore();
            },
            { enableHighAccuracy: true }
        );
    }

    findStore() {
        this.storeService.findStoreById(this.retailerId)
            .subscribe(res => {
                // console.log(res);
                this.ngZone.run(() => {
                    this.store = res;
                });
                this.initMap();
            }, error => {

            });
    }

    selectedItem(item) {
        // console.log(item);
        this.showSearchResult = false;
        this.searchText = item.address;
        this.currentPosition = {
            lng: item.location.lng,
            lat: item.location.lat,
            city: item.city
        };
        this.map.clearOverlays();
        switch (this.navId) {
            case 0:
                this.drivingRoute();
                break;
            case 1:
                this.transitRoute();
                break;
            case 2:
                this.walkingRoute();
                break;
            default:
                break;
        }
    }

    initMap() {
        // 百度地图API功能
        this.map = new BMap.Map('l-map');    // 创建Map实例
        if (this.currentPosition.lng === null) {
            return;
        }
        this.drivingRoute();
    }

    getLocation() {
        let geoc = new BMap.Geocoder();
        geoc.getLocation(new BMap.Point(Number(this.currentPosition.lng), Number(this.currentPosition.lat)), (res) => {
            this.ngZone.run(() => {
                // console.log(res);
                this.location = res;
                this.currentPosition.city = res.addressComponents.city;
                if (res.surroundingPois.length > 0) {
                    this.searchText = res.surroundingPois[0].address;
                }
                // console.log(this.searchText);
            });
        });
    }

    moveLocation() {
        let allOverlay = this.map.getOverlays();
        // console.log(allOverlay);
        allOverlay.forEach(item => {
            if (item.point && (item.point.lat === this.currentPosition.lat)) {
                // console.log(item);
                this.map.removeOverlay(item);
                return false;
            }
        });
        // 创建查询对象
        let geolocation = new BMap.Geolocation();

        // 调用getCurrentPosition函数
        geolocation.getCurrentPosition((position) => {
            if (position !== null) {
                this.currentPosition = {
                    lng: position.point.lng,
                    lat: position.point.lat,
                    city: position.address.city
                };
            }
            // console.log(position);
            // 如果查询成功
            // this.map.setZoom(16);
            // 根据point对象创建标记遮挡物，并添加到地图中
            // tslint:disable-next-line:max-line-length
            // tslint:disable-next-line:max-line-length
            let marker = new BMap.Marker(position.point, { icon: new BMap.Icon('assets/images/rolex-myself.svg', new BMap.Size(100, 100)) });
            this.map.addOverlay(marker);

            // 将地图中心设置为获得的当前位置
            this.map.panTo(position.point);
        });
    }

    // moveLocation() {
    //     if (!this.currentPosition.lat || !this.currentPosition.lng) {
    //         return;
    //     }
    //     let allOverlay = this.map.getOverlays();
    //     // console.log(allOverlay);
    //     allOverlay.forEach(item => {
    //         if (item.point && (item.point.lat === this.currentPosition.lat)) {
    //             // console.log(item);
    //             this.map.removeOverlay(item);
    //             return false;
    //         }
    //     });
    //     // this.map.setZoom(16);
    //     // 根据point对象创建标记遮挡物，并添加到地图中
    //     // tslint:disable-next-line:max-line-length
    //     setTimeout(() => {
    //         // console.log({ lat: this.currentPosition.lat, lng: this.currentPosition.lng });
    // tslint:disable-next-line:max-line-length
    //         let marker = new BMap.Marker({ lat: this.currentPosition.lat, lng: this.currentPosition.lng }, { icon: new BMap.Icon('assets/images/rolex-myself.svg', new BMap.Size(100, 100)) });
    //         this.map.addOverlay(marker);

    //         // 将地图中心设置为获得的当前位置
    //         this.map.panTo(new BMap.Point(this.currentPosition.lng, this.currentPosition.lat));
    //     }, 500);

    // }

    tapNav(index) {
        if (this.currentPosition.lng === null) {
            return;
        }
        if (this.navId === index) { return; }
        this.navId = index;
        this.map.clearOverlays();
        switch (index) {
            case 0:
                this.drivingRoute();
                break;
            case 1:
                this.transitRoute();
                break;
            case 2:
                this.walkingRoute();
                break;
            default:
                break;
        }
    }

    drivingRoute() {
        this.map.centerAndZoom(this.currentPosition.city);
        let p1 = new BMap.Point(this.currentPosition.lng, this.currentPosition.lat);
        let p2 = new BMap.Point(this.store.storeLongitude, this.store.storeLatitude);

        let driving = new BMap.DrivingRoute(this.map, {
            renderOptions: {
                map: this.map,
                panel: 'r-result',
                autoViewport: true
            },
            onSearchComplete: (result) => {
                this.ngZone.run(() => {
                    setTimeout(() => {
                        $(`#r-result`).html() === '' ? this.hasRoute = false : this.hasRoute = true;
                    }, 500);
                });
            }
        });
        driving.search(p1, p2);
        // setTimeout(() => {
        //     $(`.navtrans-view.expand`).removeAttr(`onclick`);
        // }, 2000);
    }

    transitRoute() {
        this.map.centerAndZoom(this.currentPosition.city);
        let p1 = new BMap.Point(this.currentPosition.lng, this.currentPosition.lat);
        let p2 = new BMap.Point(this.store.storeLongitude, this.store.storeLatitude);
        let transit = new BMap.TransitRoute(this.map, {
            renderOptions: {
                map: this.map,
                panel: 'r-result',
                autoViewport: true
            },
            onSearchComplete: (result) => {
                this.ngZone.run(() => {
                    setTimeout(() => {
                        $(`#r-result`).html() === '' ? this.hasRoute = false : this.hasRoute = true;
                    }, 500);
                });
            }
        });
        transit.search(p1, p2);
    }

    walkingRoute() {
        this.map.centerAndZoom(this.currentPosition.city);
        let p1 = new BMap.Point(this.currentPosition.lng, this.currentPosition.lat);
        let p2 = new BMap.Point(this.store.storeLongitude, this.store.storeLatitude);
        let walking = new BMap.WalkingRoute(this.map, {
            renderOptions: {
                map: this.map,
                panel: 'r-result',
                autoViewport: true
            },
            onSearchComplete: (result) => {
                this.ngZone.run(() => {
                    setTimeout(() => {
                        $(`#r-result`).html() === '' ? this.hasRoute = false : this.hasRoute = true;
                    }, 500);
                });
            }
        });
        walking.search(p1, p2);
    }

}
