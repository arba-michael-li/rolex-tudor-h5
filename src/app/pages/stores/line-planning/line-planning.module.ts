import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { StoreLinePlanningRoutingModule } from './line-planning-routing.module';
import { StoreLinePlanningComponent } from './line-planning.component';
import { BackButtonModule } from '../../../components';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    StoreLinePlanningRoutingModule,
    BackButtonModule
  ],
  declarations: [StoreLinePlanningComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class StoreLinePlanningModule { }
