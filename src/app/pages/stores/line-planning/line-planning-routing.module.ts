import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StoreLinePlanningComponent } from './line-planning.component';

const routes: Routes = [
  {
    path: '',
    component: StoreLinePlanningComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StoreLinePlanningRoutingModule { }
