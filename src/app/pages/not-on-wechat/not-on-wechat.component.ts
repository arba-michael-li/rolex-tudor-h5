import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Component({
    selector: 'app-not-on-wechat',
    templateUrl: './not-on-wechat.component.html',
    styleUrls: ['./not-on-wechat.component.scss']
})
export class NotOnWechatComponent implements OnInit {

    constructor(
        meta: Meta, title: Title
    ) {
        title.setTitle('抱歉，出错了');

        meta.addTags([
            {
                name: 'author', content: 'Coursetro.com'
            },
            {
                name: 'keywords', content: 'angular 6 tutorial, angular seo'
            },
            {
                name: 'description', content: 'This is my great description.'
            }
        ]);
    }

    ngOnInit() {
    }
}
