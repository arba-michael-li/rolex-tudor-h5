import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotOnWechatComponent } from './not-on-wechat.component';

const routes: Routes = [
  {
    path: '',
    component: NotOnWechatComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NotOnWechatRoutingModule { }
