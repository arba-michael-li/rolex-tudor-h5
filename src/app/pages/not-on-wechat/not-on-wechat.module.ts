import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotOnWechatRoutingModule } from './not-on-wechat-routing.module';
import { NotOnWechatComponent } from '././not-on-wechat.component';

@NgModule({
  imports: [
    CommonModule,
    NotOnWechatRoutingModule
  ],
  declarations: [NotOnWechatComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class NotOnWechatModule { }
