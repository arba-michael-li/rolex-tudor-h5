import { NgtUniversalModule } from '@ng-toolkit/universal';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { JsonpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpInterceptorService } from './services';
import { SimpleReuseStrategy } from './SimpleReuseStrategy';
import { RouteReuseStrategy } from '@angular/router';
import { WeUiModule } from 'ngx-weui';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        NgtUniversalModule,
        AppRoutingModule,
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        JsonpModule,
        WeUiModule.forRoot()
    ],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: HttpInterceptorService, multi: true },
        { provide: RouteReuseStrategy, useClass: SimpleReuseStrategy }
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class AppModule { }
