import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class WechatGuard implements CanActivate {

    constructor(private router: Router) {

    }

    canActivate() {
        // console.log('WechatGuard#canActivate called');
        if (this.isWeixin()) {
            return true;
        } else {
            this.router.navigate(['/not-on-wechat'], { replaceUrl: true });
            return false;
        }
    }

    isWeixin(): boolean {
        // return true;
        let ua: string = navigator.userAgent.toLowerCase();
        if (ua.indexOf('micromessenger') > -1) {
            return true;
        } else {
            return false;
        }
    }
}
