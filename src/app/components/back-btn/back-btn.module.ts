import { NgModule, } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BackButtonComponent } from './back-btn.component';

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        BackButtonComponent
    ],
    exports: [
        BackButtonComponent
    ]
})

export class BackButtonModule { }
