import { Component, OnInit, Input } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'BackButton',
    templateUrl: './back-btn.component.html',
    styleUrls: ['./back-btn.component.scss']
})
export class BackButtonComponent implements OnInit {
    @Input() rootUrl: string;
    @Input() styleType: string;
    @Input() color: string;

    constructor(
        private location: Location,
        private router: Router
    ) {
    }

    ngOnInit() {
        this.rootUrl = this.rootUrl || '/rolex-stores';
        this.styleType = this.styleType || 'a';
        this.color = this.color || '#666666';
        // console.log(this.rootUrl);
        // console.log(this.styleType);
    }

    goBack() {
        if ((navigator.userAgent.indexOf('MSIE') >= 0) && (navigator.userAgent.indexOf('Opera') < 0)) {
            // IE
            if (history.length > 0) {
                this.location.back();
            } else {
                this.router.navigate([this.rootUrl]);
            }
        } else {
            // 非IE浏览器
            // tslint:disable-next-line:max-line-length
            if (navigator.userAgent.indexOf('Firefox') >= 0 || navigator.userAgent.indexOf('Opera') >= 0 || navigator.userAgent.indexOf('Safari') >= 0 || navigator.userAgent.indexOf('Chrome') >= 0 || navigator.userAgent.indexOf('WebKit') >= 0) {
                if (window.history.length > 1) {
                    this.location.back();
                } else {
                    this.router.navigate([this.rootUrl]);
                }
            } else {
                // 未知的浏览器
                this.location.back();
            }
        }
    }

}
