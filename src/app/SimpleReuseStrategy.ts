import { ActivatedRouteSnapshot, DetachedRouteHandle, RouteReuseStrategy } from '@angular/router';

export class SimpleReuseStrategy implements RouteReuseStrategy {

    public static handlers: { [key: string]: DetachedRouteHandle } = {};

    private static waitDelete: string;

    public static deleteRouteSnapshot(name: string): void {
        if (SimpleReuseStrategy.handlers[name]) {
            delete SimpleReuseStrategy.handlers[name];
        } else {
            SimpleReuseStrategy.waitDelete = name;
        }
    }

    public shouldDetach(route: ActivatedRouteSnapshot): boolean {
        // console.log(route);
        if (typeof (route.queryParams.keep) !== 'undefined' && Number(route.queryParams.keep) === 0) {
            return false;
        } else {
            return true;
        }
    }


    public store(route: ActivatedRouteSnapshot, handle: DetachedRouteHandle): void {
        if (SimpleReuseStrategy.waitDelete && SimpleReuseStrategy.waitDelete === this.getRouteUrl(route)) {
            // 如果待删除是当前路由则不存储快照
            SimpleReuseStrategy.waitDelete = null;
            return;
        }
        SimpleReuseStrategy.handlers[this.getRouteUrl(route)] = handle;
    }


    public shouldAttach(route: ActivatedRouteSnapshot): boolean {
        // 在路由是login的时候清空缓存
        // console.log(route);
        // tslint:disable-next-line:max-line-length
        if (route['_routerState'].url.indexOf('/login') > -1 || (typeof (route.queryParams.keep) !== 'undefined' && Number(route.queryParams.keep) === 0)) {
            return SimpleReuseStrategy.handlers[this.getRouteUrl(route)] = null;
        }
        return !!SimpleReuseStrategy.handlers[this.getRouteUrl(route)];
    }

    /** 从缓存中获取快照，若无则返回nul */
    public retrieve(route: ActivatedRouteSnapshot): DetachedRouteHandle {
        if (!route.routeConfig) {
            return null;
        }

        return SimpleReuseStrategy.handlers[this.getRouteUrl(route)];
    }


    public shouldReuseRoute(future: ActivatedRouteSnapshot, curr: ActivatedRouteSnapshot): boolean {
        return future.routeConfig === curr.routeConfig &&
            JSON.stringify(future.params) === JSON.stringify(curr.params);
    }

    private getRouteUrl(route: ActivatedRouteSnapshot) {
        return route['_routerState'].url.replace(/\//g, '_');
    }
}
