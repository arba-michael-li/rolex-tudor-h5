import { Component, OnInit, Inject } from '@angular/core';
import { LOCAL_STORAGE, WINDOW } from '@ng-toolkit/universal';
import { Router, NavigationEnd } from '@angular/router';
import { UtilService, WXService } from './services';
import * as $ from 'jquery';

@Component({
    selector: 'app-root',
    template: `
        <router-outlet></router-outlet>
    `
})
export class AppComponent implements OnInit {
    constructor(
        @Inject(WINDOW) private window: Window,
        @Inject(LOCAL_STORAGE) private localStorage: any,
        private router: Router,
        private utilService: UtilService,
        private wxService: WXService
    ) {
        this.localStorage.removeItem(`rolex-stores-scrollTop`);
        this.localStorage.removeItem(`tudor-stores-scrollTop`);
    }

    ngOnInit(): void {
        // console.log(this.window.location.hostname);
        // console.log(this.window.location.href);
        let hostName = this.window.location.hostname;
        let href = this.window.location.href;
        if (href.indexOf('/detail/') === -1 && href.indexOf('/line-planning/') === -1) {
            if (hostName.indexOf('rolex-locationfinder.com') !== -1) {
                console.log('rolex-locationfinder.com');
                this.router.navigate(['/rolex-stores']);
            } else if (hostName.indexOf('tudor-locationfinder.com') !== -1) {
                console.log('tudor-locationfinder.com');
                this.router.navigate(['/tudor-stores']);
            } else {
                this.router.navigate(['/rolex-stores']);
            }
        }

        this.router.events.subscribe((r) => {
            if (r instanceof NavigationEnd) {
                // console.log(r);
                if (r.url.indexOf('/rolex-stores') > -1) {
                    this.scrollTop(`rolex-stores-scrollTop`, r.url);
                } else if (r.url.indexOf('/tudor-stores') > -1) {
                    this.scrollTop(`tudor-stores-scrollTop`, r.url);
                }
            }
        });

        // this.checkIsWechat();
    }

    scrollTop(key, url) {
        let scrollTop = this.localStorage.getItem(key);
        if (scrollTop && url.indexOf('keep=0') < 0) {
            $(`.page`).scrollTop(Number(scrollTop));
        }
    }

    checkIsWechat() {
        if (this.utilService.isWeixin()) {
            this.wxService.loadJSSDK('https://res.wx.qq.com/open/js/jweixin-1.0.0.js');
        }
    }
}
