import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WechatGuard } from './guards';

const routes: Routes = [
    {
        path: 'rolex-stores',
        canActivate: [WechatGuard],
        loadChildren: './pages/stores/stores.module#StoresModule'
    },
    {
        path: 'rolex-stores/detail/:retailerId',
        canActivate: [WechatGuard],
        loadChildren: './pages/stores/detail/detail.module#StoreDetailModule'
    },
    {
        path: 'rolex-stores/line-planning/:retailerId',
        canActivate: [WechatGuard],
        loadChildren: './pages/stores/line-planning/line-planning.module#StoreLinePlanningModule'
    },
    {
        path: 'tudor-stores',
        canActivate: [WechatGuard],
        loadChildren: './pages/tudor-stores/stores.module#TudorStoresModule'
    },
    {
        path: 'tudor-stores/detail/:retailerId',
        canActivate: [WechatGuard],
        loadChildren: './pages/tudor-stores/detail/detail.module#TudorStoreDetailModule'
    },
    {
        path: 'tudor-stores/line-planning/:retailerId',
        canActivate: [WechatGuard],
        loadChildren: './pages/tudor-stores/line-planning/line-planning.module#TudorStoreLinePlanningModule'
    },
    // {
    //     path: 'registration-flow2',
    //     canActivate: [WechatGuard],
    //     loadChildren: './pages/registration/registration.module#RegistrationModule'
    // },
    {
        path: 'not-on-wechat',
        loadChildren: './pages/not-on-wechat/not-on-wechat.module#NotOnWechatModule'
    },
    // {
    //     path: '',
    //     redirectTo: '/rolex-stores',
    //     pathMatch: 'full'
    // },
    // { path: '**', component: PageNotFoundComponent }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [RouterModule]
})
export class AppRoutingModule { }
